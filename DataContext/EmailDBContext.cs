﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using EmailNotifier.Model;

namespace EmailNotifier.DataContext
{
    public partial class EmailDBContext : DbContext
    {
        public EmailDBContext()
        {
        }

        public EmailDBContext(DbContextOptions<EmailDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Emailnotification> Emailnotifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
