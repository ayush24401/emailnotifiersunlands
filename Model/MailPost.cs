using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotifier.EmailModel
{
    public class MailPost
    {
        public string ProjectKey { get; set; }
        public List<List<string>> Contents { get; set; }
        public List<string> Headers { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public long Timer { get; set; }
    }
}