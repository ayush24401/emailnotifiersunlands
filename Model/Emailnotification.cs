﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace EmailNotifier.Model
{
    [Table("EMAILNOTIFICATIONS")]
    public partial class Emailnotification
    {   
        [Required]
        public string ProjectKey { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string Type { get; set; }
        public string Key1 { get; set; }
        public string Key2 { get; set; }
        public string Key3 { get; set; }
        public string Key4 { get; set; }
        public string Key5 { get; set; }
        public string Key6 { get; set; }
        public string Key7 { get; set; }
        public string Key8 { get; set; }
        public string Key9 { get; set; }
        public string Key10 { get; set; }
        public long? Flag { get; set; }
        public long? Timer { get; set; }
        [Key]
        public long SerialKey { get; set; }
    }
}
