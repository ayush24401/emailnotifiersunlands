using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotifier.Model
{
    public class EmailTemplate
    {

        public EmailTemplate()
        {
            header = new List<string>();
            mail = new List<SingleMail>();
            
        } 
       public List<string> header{get;set;}
        public List<SingleMail> mail {get;set;}
        public string subject {get;set;}
        public string Status {get;set;}
    }

    public class SingleMail
    {
        public SingleMail()
        {
            Content = new List<string>();
        }
        public int MailPk {get;set;}
        public List<string> Content {get;set;}
    }

    public class GroupedMails
    {
        public string ProjectKey { get; set; }
        public string Status { get; set; }
        public List<Emailnotification> content { get; set; }
    }
}