using System.Net;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using EmailNotifier.Details;
// using EmailNotifier.Details;

namespace EmailNotifier.Mail
{
    public static class NotifyEmail
    {


        public static void SendEmail(string Projname,List<string> header, List<List<string>> emailContent, string subject)
        {
            //-------------Email details-------------
            CommonData.setenvToSpecific(Projname);
            string From     = CommonData.From;
            string Password = CommonData.Password;
            List<string> To = CommonData.To;
            List<string> Cc = CommonData.Cc;
            string body     = CreateTable(header, emailContent);
            subject         = subject + " - " + (DateTime.Now.Date.ToString("MM/dd/yyyy"));

            MailMessage msg = new MailMessage();
            msg.From        = new MailAddress(From);
            msg.Subject     = subject;
            msg.Body        = body;
            msg.IsBodyHtml  = true;
            msg.Priority    = MailPriority.High;
            SetMsgParams(To, Cc, ref msg);

            using (SmtpClient client = new SmtpClient())
            {
                client.EnableSsl             = true;
                client.UseDefaultCredentials = false;
                client.Credentials           = new NetworkCredential(From, Password);
                client.Host                  = CommonData.Server;
                client.Port                  = CommonData.Port;
                client.DeliveryMethod        = SmtpDeliveryMethod.Network;
                client.Send(msg);

                Console.WriteLine("#Email Sent >>>> Subject: " + subject);
            }
        }
        public static void SetMsgParams(List<string> To, List<string> Cc, ref MailMessage msg)
        {
            foreach (string to in To)
            {
                msg.CC.Add(to);
            }
            foreach (string cc in Cc)
            {
                msg.CC.Add(cc);
            }
        }
        public static string CreateTable(List<string> header, List<List<string>> emailContent)
        {
            var tableString = new System.Text.StringBuilder("<table style='margin:0; border: 1px solid #000; border-collapse: collapse; font-family: Arial, Helvetica, sans-serif;'><thead style='border: 1px solid #000;'><tr style='background-color:#000; color:#FFF; border: 1px solid #FFF;'>");
            if (emailContent.Count != 0 && header.Count != emailContent[0].Count)
            {
                Console.Write("Please make sure that headers of the column and content must be of the same length");
            }
            foreach (string head in header)
            {
                tableString.Append($"<th style='border: 3px solid #000; padding: 8px 14px;'>{head}</th>");
            }
            tableString.Append("</tr></thead><tbody>");
            foreach (List<string> content in emailContent)
            {

                tableString.Append("<tr style='border: 1px solid #000; background-color: #FFF; color:#000'>");
                foreach (string c in content)
                {
                    tableString.Append($"<td style='border: 1px solid #000; padding: 8px 14px;'>{c}</td>");
                }
                tableString.Append("</tr>");
            }
            tableString.Append("</tbody></table>");
            return tableString.ToString();
        }
    }
}
