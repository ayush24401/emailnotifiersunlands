using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using EmailNotifier.DataContext;
using EmailNotifier.EmailModel;
using EmailNotifier.Mail;
using EmailNotifier.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;

namespace EmailNotifier.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmailNotificationController : Controller
    {
        private readonly EmailDBContext _eContext;
        private readonly IConfiguration _Config;

        public EmailNotificationController(EmailDBContext eContext, IConfiguration Config)
        {

            _eContext = eContext;
            _Config = Config;
        }


        //GET API
        [HttpGet("/api/sendemail/{timer}")]
        public async Task<IActionResult> send(int timer)
        {
            try
            {
                List<GroupedMails> result = GroupMails(timer);

                List<EmailTemplate> EmailsList = CastMails(result);
                System.Console.WriteLine("Email to be sent => " + result.Count);
                return await SendEmails(result, EmailsList);

            }
            catch (SmtpException ex)
            {
                System.Console.WriteLine($"FAILED SENDING MAIL \n\tMESSAGE = {ex.Message}");
                Log.Error($"FAILED SENDING MAIL \n\tMESSAGE = {ex.Message}");
                return BadRequest();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Message = " + ex.Message);
                Log.Error(ex.Message);
                return BadRequest();
            }
        }

        private async Task<IActionResult> SendEmails(List<GroupedMails> result, List<EmailTemplate> EmailsList)
        {
            List<Task<List<int>>> Tasks = new List<Task<List<int>>>();

            if (EmailsList.Count > 0)
            {

                foreach (var Email in EmailsList)
                {
                    Tasks.Add(SendProjectSpecificMail(Email, result));
                }
                List<int>[] res = await Task.WhenAll(Tasks);
                List<int> res1 = res.SelectMany(i => i).ToList();

                System.Console.WriteLine("All mail Pks to be updated" + JsonConvert.SerializeObject(res1));

                // result.Where(i=>i.ProjectKey==Email.subject && i.Status==Email.Status).ToList().ForEach(j=>j.content.ForEach(k=>k.Flag=1));
                result.SelectMany(i => i.content.Where(j => res1.Contains((int)j.SerialKey))).ToList().ForEach(k => k.Flag = 1);
                _eContext.SaveChanges();

                Log.Information($"Mails with Ids {JsonConvert.SerializeObject(res1)} Sent Successfully");

                return Ok("Mails Sent successfully!!");
            }
            else
                return Ok("No mails to send!!");
        }

        private async Task<List<int>> SendProjectSpecificMail(EmailTemplate Email, List<GroupedMails> result)
        {
            return await Task.Run(() =>
            {
                try
                {
                    string ProjName = Email.subject.Split("_").First();
                    List<int> GroupedMailPks = new List<int>();
                    var sectionExists = _Config.GetSection("EmailSettings").GetChildren().Any(item => item.Key == ProjName);
                    if (sectionExists)
                    {
                        System.Console.WriteLine("Section Exists = " + ProjName);
                        List<List<string>> MailStringList = Email.mail.Select(i => i.Content).ToList();
                        NotifyEmail.SendEmail(ProjName, Email.header, MailStringList, Email.Status.ToUpper() + " " + Email.subject);

                        //getting Pk's of all the mails which are sent successfully
                        GroupedMailPks = Email.mail.Select(i => i.MailPk).ToList();

                    }
                    return GroupedMailPks;
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex.Message);
                    return new List<int>();
                }
            });
        }

        private List<GroupedMails> GroupMails(int timer)
        {
            var result = (from econtent in _eContext.Emailnotifications
                          where (econtent.Flag != 1 || econtent.Flag == null)
                          && (econtent.Timer == timer)
                          // && econtent.Type.ToLower() == "prod"
                          // && econtent.Type.Equals("test", StringComparison.OrdinalIgnoreCase)
                          group econtent by new { econtent.ProjectKey, econtent.Status } into filtered
                          select new GroupedMails
                          {
                              ProjectKey = filtered.Key.ProjectKey,
                              Status = filtered.Key.Status,
                              content = filtered.Select(i => i).ToList()
                          }).ToList();
            return result;
        }


        private static List<EmailTemplate> CastMails(List<GroupedMails> Mails)
        {
            List<EmailTemplate> EmailsList = new List<EmailTemplate>();
            foreach (var item in Mails)
            {
                EmailTemplate Emails = new EmailTemplate();
                //ProjectKey
                Emails.subject = item.ProjectKey;
                Emails.Status = item.Status;
                System.Console.WriteLine("Proj name = " + item.ProjectKey + "\tStatus = " + item.Status + "\tCount =" + item.content.Count());

                //Contents
                foreach (var keys in item.content)
                {
                    SingleMail sd = new SingleMail();
                    //adding primarykey to each email
                    // sd.Add(keys.Sno.ToString());
                    sd.MailPk = (int)keys.SerialKey;
                    if (keys.Key1 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key1));
                    if (keys.Key2 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key2));
                    if (keys.Key3 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key3));
                    if (keys.Key4 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key4));
                    if (keys.Key5 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key5));
                    if (keys.Key6 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key6));
                    if (keys.Key7 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key7));
                    if (keys.Key8 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key8));
                    if (keys.Key9 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key9));
                    if (keys.Key10 != null) sd.Content.Add(TakeAfterFirstSpace(keys.Key10));
                    Emails.mail.Add(sd);
                }

                //Header
                Emailnotification temp = item.content.First();
                if (temp.Key1 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key1));
                if (temp.Key2 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key2));
                if (temp.Key3 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key3));
                if (temp.Key4 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key4));
                if (temp.Key5 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key5));
                if (temp.Key6 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key6));
                if (temp.Key7 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key7));
                if (temp.Key8 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key8));
                if (temp.Key9 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key9));
                if (temp.Key10 != null) Emails.header.Add(TakeBeforeFirstSpace(temp.Key10));

                EmailsList.Add(Emails);
            }

            return EmailsList;

            static string TakeAfterFirstSpace(string str)
            {
                return new string(str.SkipWhile(i => i != ' ').ToArray());
            }

            static string TakeBeforeFirstSpace(string temp)
            {
                return new string(temp.TakeWhile(i => i != ' ').ToArray());
            }
        }

        [HttpPost("/api/updatemail")]
        public IActionResult UpdateMail([FromBody] MailPost Root)
        {
            try
            {
                if (Root.Contents.All(i => i.Count() == Root.Headers.Count()))
                {
                    List<Emailnotification> Emails = new List<Emailnotification>();
                    for (int i = 0; i < Root.Contents.Count(); i++)
                    {
                        Emailnotification Mail = new Emailnotification();

                        Mail.ProjectKey = Root.ProjectKey;
                        Mail.Status = Root.Status;
                        Mail.Type = Root.Type;
                        Mail.Key1 = ExtractKey(Root, i, ref Mail, 0);
                        Mail.Key2 = ExtractKey(Root, i, ref Mail, 1);
                        Mail.Key3 = ExtractKey(Root, i, ref Mail, 2);
                        Mail.Key4 = ExtractKey(Root, i, ref Mail, 3);
                        Mail.Key5 = ExtractKey(Root, i, ref Mail, 4);
                        Mail.Key6 = ExtractKey(Root, i, ref Mail, 5);
                        Mail.Key7 = ExtractKey(Root, i, ref Mail, 6);
                        Mail.Key8 = ExtractKey(Root, i, ref Mail, 7);
                        Mail.Key9 = ExtractKey(Root, i, ref Mail, 8);
                        Mail.Key10 = ExtractKey(Root, i, ref Mail, 9);
                        Mail.Flag = 0;
                        Mail.Timer = Root.Timer;
                        Emails.Add(Mail);
                    }


                    //Update DB
                    using var Transaction = _eContext.Database.BeginTransaction();
                    try
                    {
                        _eContext.Emailnotifications.AddRange(Emails);
                        _eContext.SaveChanges();
                        Transaction.Commit();

                    }
                    catch (DbUpdateException ex)
                    {
                        Log.Error("[Mail Update Failed] -> " + ex.Message);
                        Transaction.Rollback();
                    }
                    
                }
                    return Ok("Mails Updated successfully");
            }
            catch (System.Exception ex)
            {
                Log.Error("[Error] -> " + ex.Message);
                System.Console.WriteLine("[Error] -> " + ex.Message);
                return BadRequest();
            }


            static string ExtractKey(MailPost Root, int currCounter, ref Emailnotification Mail, int index)
            {
                return Root.Headers.Count() > index ? Root.Headers[index] + " " + Root.Contents[currCounter][index] : null;
            }
        }
    }


}