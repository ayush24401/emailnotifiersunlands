using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace EmailNotifier
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try{
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(configuration)
                        .MinimumLevel.Verbose()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .MinimumLevel.Override("System", LogEventLevel.Information)
                        .Enrich.FromLogContext()
                        // .Enrich.WithMachineName()
                        // .Enrich.WithProcessId()
                        // .Enrich.WithThreadId()
                        .WriteTo.Console()
                        .WriteTo.File($"Logs/errors.txt",
                            restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error,
                            rollingInterval: RollingInterval.Day,
                            outputTemplate: "[{Timestamp:G} {Level:u3}] => {Message} {NewLine:1} {Exception:1}")
                        .WriteTo.File($"Logs/success.txt",
                            restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information,  
                            rollingInterval : RollingInterval.Day,
                            outputTemplate: "[{Timestamp:G} {Level:u3}] => {Message} {NewLine:1} {Exception:1}")
                        .CreateLogger();
            Log.Information("Email Service has started!");
            CreateHostBuilder(args).Build().Run();
            }
            catch(Exception ex)
            {
                Log.Information(ex.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
