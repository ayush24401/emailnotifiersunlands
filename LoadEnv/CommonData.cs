using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailNotifier.Details
{
    public static class CommonData
    {
        // public readonly static string  Host      = LoadEnv.AppSetting("AppSettings:Host");
        // public readonly static string  Limit     = LoadEnv.AppSetting("AppSettings:Limit");
        // public readonly static string  Schema    = LoadEnv.AppSetting("AppSettings:Schema");
        // public readonly static string  Schema2   = LoadEnv.AppSetting("AppSettings:Schema2");
        // public readonly static string  Storerkey = LoadEnv.AppSetting("AppSettings:Storerkey");
        public readonly static string  Server    = LoadEnv.AppSetting("Server");
        public readonly static int  Port         = Convert.ToInt32(LoadEnv.AppSetting("Port"));
        public static string  Password  {get;set;}
        public  static string  From      {get;set;}
        public  static List<string>  To  {get;set;}
        public  static List<string>  Cc  {get;set;}
        public static List<string> GetStringList(string projname,string name)
        {
            List<string> List = LoadEnv.
                                AppSetting(projname+":"+name).
                                Split(",").
                                ToList();
            return List;
        }

        public static void setenvToSpecific(string Projname)
        {
            System.Console.WriteLine($"Changing Email specs to {Projname}");
            CommonData.Password = LoadEnv.AppSetting($"{Projname}:Password");
            CommonData.From = LoadEnv.AppSetting($"{Projname}:From");
            CommonData.To = GetStringList(Projname,"To");
            CommonData.Cc = GetStringList(Projname,"Cc");
        }


    }


}
