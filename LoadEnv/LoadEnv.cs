using System.IO;
using Microsoft.Extensions.Configuration;

namespace EmailNotifier.Details
{
    public class LoadEnv
    {
        private static LoadEnv _appSettings;
        public string appSettingValue { get; set; }
        public static string AppSetting(string Key)
        {
            _appSettings = GetCurrentSettings(Key);
            return _appSettings.appSettingValue;
        }
        public LoadEnv(IConfiguration config, string Key)
        {
            this.appSettingValue = config.GetValue<string>(Key);
        }
        // Get a valued stored in the appsettings.
        // Pass in a key like TestArea:TestKey to get TestValue
        public static LoadEnv GetCurrentSettings(string Key)
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            // .AddJsonFile("/opt/byteally/delta-faucet/appsettings.json", optional: false, reloadOnChange: true)
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                            .AddEnvironmentVariables();
            IConfigurationRoot configuration = builder.Build();

            var settings = new LoadEnv(configuration.GetSection("EmailSettings"), Key);
            return settings;
        }
        
    }
}